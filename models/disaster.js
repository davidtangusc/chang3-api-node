const bookshelf = require('./../config/bookshelf');

module.exports = bookshelf.Model.extend({
  tableName: 'disasters'
}, {
  async fetchNames() {
    let disasters = await this.fetchAll();
    return disasters.map((disaster) => {
      return disaster.get('name').toLowerCase();
    });
  }
});