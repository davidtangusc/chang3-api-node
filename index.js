require('dotenv').config();

const Koa = require('koa');
const Router = require('koa-router');
const cors = require('koa-cors');
const bodyParser = require('koa-bodyparser');
const nlp = require('compromise');
const Disaster = require('./models/disaster');

const app = new Koa();
const router = new Router();

app.use(cors());
app.use(bodyParser({
  enableTypes: ['text']
}));

router.post('/api/article-verification', async (ctx) => {
  let disasterNames = await Disaster.fetchNames();
  let disasterNamesSet = new Set(disasterNames);

  let breakdown = nlp(ctx.request.body)
    .nouns()
    .out('frequency');

  let found = breakdown.find((result) => {
    return disasterNamesSet.has(result.normal);
  });

  ctx.body = found;
});

app
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(process.env.PORT || 8000);
